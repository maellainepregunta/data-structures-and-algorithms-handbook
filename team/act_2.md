# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring IT security specialist

## Maria Ellaine B. Pregunta

🤪 Aspiring IT security specialist! 🕵️‍♀️ —  📧maellainepregunta@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_pregunta_ellaine.jpg](images/act_2_pregunta_ellaine.jpg)

### Bio

**Good to know:** Trying to do better.

**Motto:** It's okay to be nervous because we have nervous system.

**Languages:** Java

**Other Technologies:** Pinterest

**Personality Type:** [Mediator (INFP-T)](https://www.16personalities.com/profiles/c5ea44cd4a89a)


<!-- END -->
