# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Game Developer

## Christian Visaya

👋 Aspiring Game Developer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** Good to know

**Motto:** Motto

**Languages:** Go

**Other Technologies:** DevOps

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)
<!-- END -->
# Aspiring CEO

## Jerome Imperial

👋 Future CEO!🤵✈️⚽— 💌 jeromeimperial@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_jerome_imperial.jpg](images/bsis_2_jerome_imperial.jpg)

### Bio

**Good to know:** I have little fear when aiming for towering goals. I might fail, but with God's help, my 'failure' (if you can call it that) will be better than most successes. Nonetheless, I pray for everyone's wellness 😉.

**Motto:** Obsession beats talent🏆🐢..................🐇

**Languages:** Python, Java, C++, SQL

**Other Technologies:** MS Excel, Anaconda's Jupyter Notebook, Arduino UNO

**Personality Type:** [Architect (INTJ-T)](https://www.16personalities.com/profiles/249d8eea60d58) from [Advocate (INFJ-T) ](https://www.16personalities.com/profiles/0d91b1539f64a) 3 years ago.

<!-- END -->